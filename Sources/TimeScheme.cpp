// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //


#include "TimeScheme.hpp"

#include "Utilities/String/EmptyString.hpp"


namespace MoReFEM::AcousticWaveNS
{


    namespace InputDataNS
    {


        const std::string& TimeScheme::GetName()
        {
            static std::string ret("TimeScheme");
            return ret;
        }

        const std::string& TimeScheme::Theta::NameInFile()
        {
            static std::string ret("Theta");
            return ret;
        }


        const std::string& TimeScheme::Theta::Description()
        {
            static std::string ret("Theta parameter in the theta-scheme.");
            return ret;
        }

        const std::string& TimeScheme::Theta::Constraint()
        {
            return Utilities::EmptyString();
        }


        const std::string& TimeScheme::Theta::DefaultValue()
        {
            static std::string ret("0.");
            return ret;
        }


    } // namespace InputDataNS


} // namespace MoReFEM::AcousticWaveNS
