// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //

// > *** MoReFEM4AcousticWave header guards *** //
#ifndef MOREFEM4ACOUSTICWAVE_INPUTDATA_DOT_HPP_
#define MOREFEM4ACOUSTICWAVE_INPUTDATA_DOT_HPP_
// *** MoReFEM4AcousticWave header guards *** < //


#include "Utilities/Containers/EnumClass.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
#include "Core/InputData/Instances/Parameter/Source/RectangularSourceTimeParameter.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Instances.hpp"

#include "TimeScheme.hpp"


namespace MoReFEM::AcousticWaveNS
{


    enum class MeshIndex : std::size_t { mesh = 1 };


    enum class DomainIndex : std::size_t { highest_dimension = 1, dirichlet = 2, full_mesh = 3 };


    enum class BoundaryConditionIndex : std::size_t { first = 1 };


    enum class FEltSpaceIndex : std::size_t { highest_dimension = 1 };


    enum class UnknownIndex : std::size_t { velocity = 1 };


    enum class SolverIndex : std::size_t { solver = 1 };


    enum class NumberingSubsetIndex : std::size_t { monolithic = 1 };

    enum class TensorIndex : std::size_t { diffusion_tensor = 1 };


    enum class InitialConditionIndex : std::size_t { velocity_initial_condition1 = 1, velocity_initial_condition2 = 2 };


    enum class SourceIndex : std::size_t { volumic_source = 1, time_dependent_source = 2 };


    enum class RectangularSourceTimeParameterIndex : std::size_t {
        volumic_source = 1,
    };


    template<ParameterNS::Type TypeT, unsigned int IndexT>
    using source_type = std::conditional_t<TypeT == ParameterNS::Type::scalar,
                                           ::MoReFEM::InputDataNS::ScalarTransientSource<IndexT>,
                                           ::MoReFEM::InputDataNS::VectorialTransientSource<IndexT>>;


    // clang-format off
    template<ParameterNS::Type TypeT>
    using input_data_tuple = std::tuple<
        ::MoReFEM::InputDataNS::TimeManager,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::first),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver),

        source_type<TypeT, EnumUnderlyingType(SourceIndex::volumic_source)>,
        source_type<TypeT, EnumUnderlyingType(SourceIndex::time_dependent_source)>,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::velocity_initial_condition1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::velocity_initial_condition2),

        ::MoReFEM::InputDataNS::Diffusion::Density,
        ::MoReFEM::InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>,

        ::MoReFEM::InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
            RectangularSourceTimeParameterIndex::volumic_source)>,

        InputDataNS::TimeScheme,

        ::MoReFEM::InputDataNS::Result>;


    template<ParameterNS::Type TypeT>
    using input_data_type = InputData<input_data_tuple<TypeT>>;


    template<ParameterNS::Type TypeT>
    using model_settings_tuple =
    std::tuple<
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::monolithic),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::velocity),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::first),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver),

        typename source_type<TypeT, EnumUnderlyingType(SourceIndex::volumic_source)>::IndexedSectionDescription,
        typename source_type<TypeT, EnumUnderlyingType(SourceIndex::time_dependent_source)>::IndexedSectionDescription,

        ::MoReFEM::InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
            TensorIndex::diffusion_tensor)>::IndexedSectionDescription,

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::velocity_initial_condition1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::velocity_initial_condition2),

        ::MoReFEM::InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
            RectangularSourceTimeParameterIndex::volumic_source)>::IndexedSectionDescription
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    template<ParameterNS::Type TypeT>
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple<TypeT>>
    {
        //! Convenient alias.
        using parent = ::MoReFEM::ModelSettings<model_settings_tuple<TypeT>>;

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type =
        ::MoReFEM::TimeManagerNS::Instance::ConstantTimeStep<TimeManagerNS::support_restart_mode::no>;


    //! \copydoc doxygen_hide_morefem_data_type
    template<ParameterNS::Type TypeT>
    using morefem_data_type =
    MoReFEMData<ModelSettings<TypeT>, input_data_type<TypeT>, time_manager_type, program_type::model>;


} // namespace MoReFEM::AcousticWaveNS


#include "ModelSettings.hxx"


// > *** MoReFEM4AcousticWave end header guards *** //
#endif // MOREFEM4ACOUSTICWAVE_INPUTDATA_DOT_HPP_
// *** MoReFEM4AcousticWave end header guards *** < //
