// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //


#include <cstddef> // IWYU pragma: keep

#include "Model/Main/MainEnsightOutput.hpp"

#include "InputData.hpp"
#include "Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::AcousticWaveNS;


int main(int argc, char** argv)
{
    constexpr auto parameter_type = ParameterNS::Type::scalar;

    std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId(
        NumberingSubsetIndex::monolithic) };

    std::vector<std::string> unknown_list{ "velocity" };

    return ModelNS::MainEnsightOutput<AcousticWaveNS::Model<parameter_type>>(
        argc, argv, AsMeshId(MeshIndex::mesh), numbering_subset_id_list, unknown_list);
}
