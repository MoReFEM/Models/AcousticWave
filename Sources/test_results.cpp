// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //


#define BOOST_TEST_MODULE model_acoustic_wave
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "InputData.hpp"

using namespace MoReFEM;

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& scalar_or_vectorial);


    struct Fixture : public TestNS::FixtureNS::TestEnvironment
    {
        Fixture();
    };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(scalar_sequential, Fixture)
{
    CommonTestCase("Seq", "Scalar");
}


BOOST_FIXTURE_TEST_CASE(scalar_mpi4, Fixture)
{
    CommonTestCase("Mpi4", "Scalar");
}


BOOST_FIXTURE_TEST_CASE(vectorial_sequential, Fixture)
{
    CommonTestCase("Seq", "Vectorial");
}


BOOST_FIXTURE_TEST_CASE(vectorial_mpi4, Fixture)
{
    CommonTestCase("Mpi4", "Vectorial");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& scalar_or_vectorial)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        std::string root_dir_path, output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path = environment.GetEnvironmentVariable("MODEL_ROOT_DIRECTORY"));
        /* BOOST_REQUIRE_NO_THROW */ (output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR"));

        FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);
        BOOST_CHECK(output_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(root_dir,
                                        std::vector<std::string>{ "Sources", "ExpectedResults", scalar_or_vectorial });

        FilesystemNS::Directory obtained_dir(
            output_dir, std::vector<std::string>{ seq_or_par, "Ascii", scalar_or_vectorial, "Rank_0" });


        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata");

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata");

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case");

        std::ostringstream oconv;

        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        constexpr auto epsilon = 1.e-15; // default epsilon makes the test fail.

        for (auto i = 0; i < 5; ++i)
        {
            oconv.str("");
            oconv << "velocity." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, oconv.str(), epsilon);
        }
    }


    Fixture::Fixture()
    {
        static bool first_call = true;

        if (first_call)
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();
            decltype(auto) cli_args = boost::unit_test::framework::master_test_suite().argv;

            environment.SetEnvironmentVariable(std::make_pair("MODEL_ROOT_DIRECTORY", cli_args[1]));

            first_call = false;
        }
    }


} // namespace
