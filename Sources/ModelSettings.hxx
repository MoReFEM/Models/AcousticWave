// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //

// > *** MoReFEM4AcousticWave header guards *** //
#ifndef MOREFEM4ACOUSTICWAVE_MODELSETTINGS_DOT_HXX_
#define MOREFEM4ACOUSTICWAVE_MODELSETTINGS_DOT_HXX_
// IWYU pragma: private, include "ModelSettings.hpp"
// *** MoReFEM4AcousticWave header guards *** < //

namespace MoReFEM::AcousticWaveNS
{

    template<ParameterNS::Type TypeT>
    void ModelSettings<TypeT>::Init()
    {
        // ****** Numbering subset ******
        {
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>>(
                "Monolithic");
        }

        // ****** Unknown ******
        {
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>>({ "Velocity" });

            parent::template Add<::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>::Name>(
                "velocity");

            if constexpr (TypeT == ParameterNS::Type::scalar)
                parent::template Add<
                    ::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>::Nature>("scalar");
            else
                parent::template Add<
                    ::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>::Nature>("vectorial");
        }

        // ****** Domain ******
        {
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
                { "Highest dimension geometric elements" });
            parent::template SetDescription<::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>>(
                { "Domain upon which Dirichlet boundary condition is applied" });
            parent::template SetDescription<::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>(
                { "Full mesh" });

            parent::template Add<
                ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            parent::template Add<
                ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            parent::template Add<
                ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
        }

        // ****** Finite element space ******
        {
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
                "Finite element space for highest geometric dimension");

            parent::template Add<::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(
                FEltSpaceIndex::highest_dimension)>::GodOfDofIndex>(EnumUnderlyingType(MeshIndex::mesh));
            parent::template Add<
                ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::highest_dimension));
            parent::template Add<
                ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::UnknownList>(
                { "velocity" });
            parent::template Add<::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(
                FEltSpaceIndex::highest_dimension)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });
        }

        // ****** Dirichlet boundary condition ******
        {
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>>(
                { "Sole Dirichlet boundary condition" });

            parent::template Add<::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::first)>::UnknownName>("velocity");
            parent::template Add<::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::first)>::DomainIndex>(EnumUnderlyingType(DomainIndex::dirichlet));
            parent::template Add<::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::first)>::IsMutable>(false);
        }

        // ****** Initial condition ******
        {
            parent::template SetDescription<::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(
                InitialConditionIndex::velocity_initial_condition1)>>({ "First initial condition" });
            parent::template SetDescription<::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(
                InitialConditionIndex::velocity_initial_condition2)>>({ "Second initial condition" });
        }


        parent::template SetDescription<::MoReFEM::InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>(
            { "Sole mesh" });


        if constexpr (TypeT == ParameterNS::Type::scalar)
        {
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::ScalarTransientSource<EnumUnderlyingType(SourceIndex::volumic_source)>>(
                { "Volumic scalar source" });
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::ScalarTransientSource<EnumUnderlyingType(SourceIndex::time_dependent_source)>>(
                { "Time-dependant scalar source" });
        } else
        {
            parent::template SetDescription<
                ::MoReFEM::InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::volumic_source)>>(
                { "Volumic vectorial source" });
            parent::template SetDescription<::MoReFEM::InputDataNS::VectorialTransientSource<EnumUnderlyingType(
                SourceIndex::time_dependent_source)>>({ "Time-dependant vectorial source" });
        }

        parent::template SetDescription<::MoReFEM::InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>(
            { "Linear solver" });

        parent::template SetDescription<
            ::MoReFEM::InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>>(
            { "Diffusion tensor" });

        parent::template SetDescription<::MoReFEM::InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
            RectangularSourceTimeParameterIndex::volumic_source)>>("Rectangular source time parameter");
    }


} // namespace MoReFEM::AcousticWaveNS

// > *** MoReFEM4AcousticWave end header guards *** //
#endif // MOREFEM4ACOUSTICWAVE_MODELSETTINGS_DOT_HXX_
// *** MoReFEM4AcousticWave end header guards *** < //
