// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //

// > *** MoReFEM4AcousticWave header guards *** //
#ifndef MOREFEM4ACOUSTICWAVE_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM4ACOUSTICWAVE_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM4AcousticWave header guards *** < //

#include <memory>
#include <vector>

#include "Geometry/Domain/Domain.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Internal/ParameterInstance.hpp"
#include "Parameters/TimeDependency/TimeDependency.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "FormulationSolver/VariationalFormulation.hpp"

#include "InputData.hpp"
#include "TimeScheme.hpp"


namespace MoReFEM::AcousticWaveNS
{


    /*!
     * \brief Variational formulation used in acoustic wave model.
     *
     * \tparam TypeT Whether the unknown is scalar or vectorial.
     */
    template<ParameterNS::Type TypeT>
    class VariationalFormulation : public MoReFEM::VariationalFormulation<VariationalFormulation<TypeT>,
                                                                          EnumUnderlyingType(SolverIndex::solver),
                                                                          time_manager_type>
    {
      private:
        static_assert(TypeT != ParameterNS::Type::matrix, "Irrelevant here.");

        //! \copydoc doxygen_hide_alias_self
        using self = VariationalFormulation<TypeT>;

        //! Alias to the parent class.
        using parent =
            MoReFEM::VariationalFormulation<self, EnumUnderlyingType(SolverIndex::solver), time_manager_type>;


        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to source operator.
        using source_operator_type = GlobalVariationalOperatorNS::TransientSource<TypeT, time_manager_type>;

        //! Alias to parameter type with time dependency.
        using time_dependent_parameter_type =
            Parameter<TypeT, LocalCoords, time_manager_type, ParameterNS::TimeDependencyFunctor>;

        //! Alias to parameter type with no time dependency.
        using no_time_dep_param_type =
            Parameter<TypeT, LocalCoords, time_manager_type, ParameterNS::TimeDependencyNS::None>;

        //! Alias to transient source operator.
        using source_op_type =
            GlobalVariationalOperatorNS::TransientSource<TypeT, time_manager_type, ParameterNS::TimeDependencyFunctor>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         */
        explicit VariationalFormulation(const NumberingSubset& numbering_subset,
                                        const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type<TypeT>& morefem_data);

        //! Destructor.
        virtual ~VariationalFormulation() override = default;

        //! Copy constructor.
        VariationalFormulation(const VariationalFormulation&) = delete;

        //! Move constructor.
        VariationalFormulation(VariationalFormulation&&) = delete;

        //! Copy affectation.
        VariationalFormulation& operator=(const VariationalFormulation&) = delete;

        //! Move affectation.
        VariationalFormulation& operator=(VariationalFormulation&&) = delete;

        ///@}


        //! At each time iteration, compute the system Rhs.
        void ComputeDynamicSystemRhs();

        /*!
         * \brief Get the only numbering subset relevant for this VariationalFormulation.
         *
         * There is a more generic accessor in the base class but is use is more unwieldy.
         */
        const NumberingSubset& GetNumberingSubset() const;

        //! Exchange the different solutions at each time step n+1->n and n->n-1.
        void UpdateSolutionForNextTimeStep();

        //! Compute the energy at each time step.
        void ComputeEnergy();

        //! Constant accessor to the underlying Parameter used to define source operator.
        const time_dependent_parameter_type& GetTimeDependentSourceParameter() const noexcept;

        //! Non constant accessor to the underlying Parameter used to define source operator.
        time_dependent_parameter_type& GetNonCstTimeDependentSourceParameter() noexcept;


      private:
        /// \name CRTP-required methods.
        ///@{

        /*!
         * \brief Specific initialisation for derived class attributes.
         *
         * \internal <b><tt>[internal]</tt></b> This method is called by base class method VariationalFormulation::Init().
         */
        void SupplInit(const morefem_data_type<TypeT>& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();

        //! Define the pointer function required to calculate the function required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESFunction ImplementSnesFunction() const;

        //! Define the pointer function required to calculate the jacobian required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESJacobian ImplementSnesJacobian() const;

        //! Define the pointer function required to view the results required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESViewer ImplementSnesViewer() const;

        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

        ///@}
      private:
        /*!
         * \brief Define the properties of all the global variational operators involved.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         */
        void DefineOperators(const morefem_data_type<TypeT>& morefem_data);

        /*!
         * \brief Assemble method for all the static operators.
         */
        void AssembleStaticOperators();

        //! Compute the System matrix ie add all the matrices in system matrix solution.
        void ComputeSystemMatrix();

      private:
        /// \name Global variational operators.
        ///@{

        //! Mass operator
        GlobalVariationalOperatorNS::Mass::const_unique_ptr mass_operator_ = nullptr;

        //! stiffness operator.
        GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr stiffness_operator_ = nullptr;

        //! Volumic source operator.
        typename source_operator_type::const_unique_ptr volumic_source_operator_ = nullptr;

        //! Time Dependent Source operator.
        typename source_op_type::const_unique_ptr time_dependent_source_operator_ = nullptr;

        ///@}

        //! Get the volumic source operator.
        const source_operator_type& GetVolumicSourceOperator() const noexcept;

        //! Constant accessor to the source operator.
        const source_op_type& GetTimeDependentSourceOperator() const noexcept;

        //! Non constant accessor to the source operator.
        source_op_type& GetNonCstTimeDependentSourceOperator() noexcept;

        //! Get the stiffness operator.
        const GlobalVariationalOperatorNS::GradPhiGradPhi& GetStiffnessOperator() const noexcept;

        //! Get the mass operator.
        const GlobalVariationalOperatorNS::Mass& GetMassOperator() const noexcept;


      private:
        /// \name Global vectors and matrices specific to the problem.
        ///@{


        //! current volumic source vector.
        GlobalVector::unique_ptr vector_current_volumic_source_ = nullptr;

        //! current time_dependent source vector.
        GlobalVector::unique_ptr vector_current_time_dependent_source_ = nullptr;

        //! Matrix current conductivity.
        GlobalMatrix::unique_ptr matrix_stiffness_ = nullptr;

        //! Current capacity per time matrix.
        GlobalMatrix::unique_ptr matrix_mass_per_square_time_step_ = nullptr;

        //! Vector containing the solution at n-1.
        GlobalVector::unique_ptr system_solution_two_iterations_before_ = nullptr;

        //! Vector containing the solution (Un+1 - Un)/dt. Used for the energy computation.
        GlobalVector::unique_ptr diff_system_solution_ = nullptr;

        //! Vector containing the solution (Un+1 + Un)/2. Used for the energy computation.
        GlobalVector::unique_ptr midpoint_system_solution_ = nullptr;

        ///@}

        const GlobalVector& GetVectorCurrentVolumicSource() const;

        GlobalVector& GetNonCstVectorCurrentVolumicSource();

        const GlobalVector& GetVectorCurrentTimeDependentSource() const;

        GlobalVector& GetNonCstVectorCurrentTimeDependentSource();

        const GlobalMatrix& GetMatrixStiffness() const;

        GlobalMatrix& GetNonCstMatrixStiffness();

        GlobalMatrix& GetNonCstMatrixMassPerSquareTimeStep();

        const GlobalMatrix& GetMatrixMassPerSquareTimeStep() const;

        const GlobalVector& GetVectorSystemSolutionTwoIterationsBefore() const;

        GlobalVector& GetNonCstVectorSystemSolutionTwoIterationsBefore();

        const GlobalVector& GetVectorDiffSystemSolution() const;

        GlobalVector& GetNonCstVectorDiffSystemSolution();

        const GlobalVector& GetVectorMidpointSystemSolution() const;

        GlobalVector& GetNonCstVectorMidpointSystemSolution();

      private:
        //! Acces to inital_time_volumic_source.
        double GetInitialTimeVolumicSource() const noexcept;

        //! Acces to final_time_volumic_source.
        double GetFinalTimeVolumicSource() const noexcept;

      private:
        /// \name Material parameters.
        ///@{

        //! Diffusion tensor.
        typename ScalarParameter<time_manager_type>::unique_ptr diffusion_tensor_ = nullptr;

        //! Diffusion Density.
        typename ScalarParameter<time_manager_type>::unique_ptr density_ = nullptr;

        ///@}

        //! Diffusion density,
        const ScalarParameter<time_manager_type>& GetDensity() const;

        //! Diffusion tensor.
        const ScalarParameter<time_manager_type>& GetDiffusionTensor() const;


      private:
        /// \name Parameters used to define TransientSource operators.
        ///@{

        //! Volumic source parameter.
        typename no_time_dep_param_type::unique_ptr volumic_source_parameter_ = nullptr;

        //! Underlying Parameter used to define source operator.
        typename time_dependent_parameter_type::unique_ptr time_dependent_source_parameter_ = nullptr;


        ///@}

        //! Constant access to the volumic source parameter.
        const no_time_dep_param_type& GetVolumicSourceParameter() const;


      private:
        //! Numbering subset used in the formulation.
        const NumberingSubset& numbering_subset_;

      private:
        /// \name Parameter theta for the time scheme used.
        ///@{

        double theta_;

        ///@}

        double GetTheta() const noexcept;

      private:
        double initial_time_volumic_source_;

        double final_time_volumic_source_;

      private:
        //! Energy stored at current time.
        double midpoint_energy_;
    };


} // namespace MoReFEM::AcousticWaveNS


#include "VariationalFormulation.hxx"

// > *** MoReFEM4AcousticWave end header guards *** //
#endif // MOREFEM4ACOUSTICWAVE_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM4AcousticWave end header guards *** < //
