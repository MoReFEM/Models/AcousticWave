// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //

// > *** MoReFEM4AcousticWave header guards *** //
#ifndef MOREFEM4ACOUSTICWAVE_TIMESCHEME_DOT_HPP_
#define MOREFEM4ACOUSTICWAVE_TIMESCHEME_DOT_HPP_
// *** MoReFEM4AcousticWave header guards *** < //

#include "Utilities/InputData/Advanced/Crtp/Section.hpp"

#include "Core/InputData/Instances/Parameter/Internal/ParameterFields.hpp"


namespace MoReFEM::AcousticWaveNS::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct TimeScheme
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<TimeScheme, Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input parameter.
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = TimeScheme;

        //! Friendship to section parent.
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, Advanced::InputDataNS::NoEnclosingSection>;
        friend parent;


        /*!
         * \brief Choose how is described the  (through a scalar, a function, etc...)
         */
        struct Theta : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Theta, self, double>
        {

            //! Name of the input parameter in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input parameter.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a LuaOptionFile one. Hereafter some text from
             * \a LuaOptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a LuaOptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has
             * been given in the input parameter file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        using section_content_type = std::tuple<Theta>;

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct TimeScheme


} // namespace MoReFEM::AcousticWaveNS::InputDataNS


// > *** MoReFEM4AcousticWave end header guards *** //
#endif // MOREFEM4ACOUSTICWAVE_TIMESCHEME_DOT_HPP_
// *** MoReFEM4AcousticWave end header guards *** < //
