// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //

// > *** MoReFEM4AcousticWave header guards *** //
#ifndef MOREFEM4ACOUSTICWAVE_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM4ACOUSTICWAVE_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "VariationalFormulation.hpp"
// *** MoReFEM4AcousticWave header guards *** < //

namespace MoReFEM::AcousticWaveNS
{


    template<ParameterNS::Type TypeT>
    VariationalFormulation<TypeT>::VariationalFormulation(
        const NumberingSubset& numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type<TypeT>& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data), numbering_subset_(numbering_subset)
    { }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::AllocateMatricesAndVectors()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();

        parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
        parent::AllocateSystemVector(numbering_subset);

        const GlobalMatrix& system_matrix = parent::GetSystemMatrix(numbering_subset, numbering_subset);
        const GlobalVector& system_rhs = parent::GetSystemRhs(numbering_subset);

        vector_current_volumic_source_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_time_dependent_source_ = std::make_unique<GlobalVector>(system_rhs);

        matrix_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
        matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);
        system_solution_two_iterations_before_ = std::make_unique<GlobalVector>(system_rhs);

        diff_system_solution_ = std::make_unique<GlobalVector>(system_rhs);
        midpoint_system_solution_ = std::make_unique<GlobalVector>(system_rhs);
    }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::SupplInit(const morefem_data_type<TypeT>& morefem_data)
    {
        using Diffusion = ::MoReFEM::InputDataNS::Diffusion;

        const GodOfDof& god_of_dof = parent::GetGodOfDof();

        decltype(auto) domain_manager = DomainManager::GetInstance();

        decltype(auto) domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_mesh));

        diffusion_tensor_ =
            InitScalarParameterFromInputData<Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>,
                                             time_manager_type>("Diffusion tensor", domain, morefem_data);

        density_ =
            InitScalarParameterFromInputData<Diffusion::Density, time_manager_type>("Density", domain, morefem_data);

        midpoint_energy_ = 0.;

        if (!GetDiffusionTensor().IsConstant())
            throw Exception("Current acoustic wave model is restricted to a constant diffusion tensor.");


        if (!GetDensity().IsConstant())
            throw Exception("Current acoustic wave model is restricted to a constant density.");

        theta_ = ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::TimeScheme::Theta>(morefem_data);

        using RectangularSourceTimeParameter1 =
            ::MoReFEM::InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
                RectangularSourceTimeParameterIndex::volumic_source)>;

        initial_time_volumic_source_ =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename RectangularSourceTimeParameter1::InitialTimeOfActivationList>(
                morefem_data);
        final_time_volumic_source_ =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename RectangularSourceTimeParameter1::FinalTimeOfActivationList>(
                morefem_data);

        DefineOperators(morefem_data);
        AssembleStaticOperators();
        ComputeSystemMatrix();

        const NumberingSubset& numbering_subset = GetNumberingSubset();
        GlobalVector& system_solution_two_iterations_before = GetNonCstVectorSystemSolutionTwoIterationsBefore();

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const Unknown& velocity = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::velocity));
        const FEltSpace& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        parent::template ApplyInitialConditionToVector<EnumUnderlyingType(
            InitialConditionIndex::velocity_initial_condition1)>(morefem_data,
                                                                 numbering_subset,
                                                                 velocity,
                                                                 felt_space_highest_dimension,
                                                                 system_solution_two_iterations_before);

        parent::template SetInitialSystemSolution<EnumUnderlyingType(
            InitialConditionIndex::velocity_initial_condition2)>(
            morefem_data, numbering_subset, velocity, felt_space_highest_dimension);
    }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::DefineOperators(const morefem_data_type<TypeT>& morefem_data)
    {
        const GodOfDof& god_of_dof = parent::GetGodOfDof();
        const FEltSpace& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        decltype(auto) velocity_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::velocity));

        namespace GVO = GlobalVariationalOperatorNS;

        using parameter_type = source_type<TypeT, EnumUnderlyingType(SourceIndex::volumic_source)>;

        decltype(auto) domain_manager = DomainManager::GetInstance();

        decltype(auto) domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_mesh));

        using parameter_type_2 = source_type<TypeT, EnumUnderlyingType(SourceIndex::time_dependent_source)>;


        if constexpr (TypeT == ParameterNS::Type::scalar)
        {
            volumic_source_parameter_ = InitScalarParameterFromInputData<parameter_type, time_manager_type>(
                "Volumic source", domain, morefem_data);

            time_dependent_source_parameter_ = InitScalarParameterFromInputData<parameter_type_2,
                                                                                time_manager_type,
                                                                                ParameterNS::TimeDependencyFunctor>(
                "Time dependent Source", domain, morefem_data);
        } else if (TypeT == ParameterNS::Type::vector)
        {
            volumic_source_parameter_ = Init3DCompoundParameterFromInputData<parameter_type, time_manager_type>(
                "Volumic source", domain, morefem_data);

            time_dependent_source_parameter_ = Init3DCompoundParameterFromInputData<parameter_type_2,
                                                                                    time_manager_type,
                                                                                    ParameterNS::TimeDependencyFunctor>(
                "Time dependent Source", domain, morefem_data);
        } else
        {
            assert(false && "No other cases supported by this model.");
            exit(EXIT_FAILURE);
        }


        const auto& volumic_source_parameter = GetVolumicSourceParameter();

        volumic_source_operator_ = std::make_unique<source_operator_type>(
            felt_space_highest_dimension, velocity_ptr, volumic_source_parameter);

        assert(!(!time_dependent_source_parameter_));

        {
            auto linear = [](double time)
            {
                return time;
            };

            auto time_dep = std::make_unique<ParameterNS::TimeDependencyFunctor<TypeT, time_manager_type>>(
                parent::GetTimeManager(), std::move(linear));

            this->GetNonCstTimeDependentSourceParameter().SetTimeDependency(std::move(time_dep));
        }

        using operator_type = typename decltype(time_dependent_source_operator_)::element_type;

        time_dependent_source_operator_ = std::make_unique<operator_type>(
            felt_space_highest_dimension, velocity_ptr, GetNonCstTimeDependentSourceParameter());

        stiffness_operator_ =
            std::make_unique<GVO::GradPhiGradPhi>(felt_space_highest_dimension, velocity_ptr, velocity_ptr);

        mass_operator_ = std::make_unique<GVO::Mass>(felt_space_highest_dimension, velocity_ptr, velocity_ptr);
    }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::AssembleStaticOperators()
    {
        GlobalMatrix& mass_matrix = GetNonCstMatrixMassPerSquareTimeStep();
        mass_matrix.ZeroEntries();

        const double density = GetDensity().GetConstantValue();

        const double time_step = parent::GetTimeManager().GetTimeStep();

        const double mass_coefficient = density / (time_step * time_step);

        const GlobalVariationalOperatorNS::Mass& mass_operator = GetMassOperator();

        {
            GlobalMatrixWithCoefficient matrix(mass_matrix, mass_coefficient);
            mass_operator.Assemble(std::make_tuple(std::ref(matrix)));
        }

        GlobalMatrix& stiffness_matrix = GetNonCstMatrixStiffness();
        stiffness_matrix.ZeroEntries();

        const GlobalVariationalOperatorNS::GradPhiGradPhi& stiffness_operator = GetStiffnessOperator();
        const double diffusion_tensor = GetDiffusionTensor().GetConstantValue();
        const double stiffness_coefficient = diffusion_tensor;

        {
            GlobalMatrixWithCoefficient matrix(stiffness_matrix, stiffness_coefficient);
            stiffness_operator.Assemble(std::make_tuple(std::ref(matrix)));
        }
    }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::ComputeSystemMatrix()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();

        GlobalMatrix& system_matrix = parent::GetNonCstSystemMatrix(numbering_subset, numbering_subset);

        const GlobalMatrix& mass_matrix = GetMatrixMassPerSquareTimeStep();
        const GlobalMatrix& stiffness_matrix = GetMatrixStiffness();

        system_matrix.Copy(mass_matrix);

        const double theta = GetTheta();

#ifndef NDEBUG
        AssertSameNumberingSubset(stiffness_matrix, system_matrix);
#endif // NDEBUG

        Wrappers::Petsc::AXPY<NonZeroPattern::same>(theta, stiffness_matrix, system_matrix);
    }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::ComputeDynamicSystemRhs()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();

        GlobalVector& rhs = parent::GetNonCstSystemRhs(numbering_subset);

        const double time = parent::GetTimeManager().GetTime();

        GlobalVector& vector_current_volumic_source = GetNonCstVectorCurrentVolumicSource();
        vector_current_volumic_source.ZeroEntries();

        if (time > GetInitialTimeVolumicSource() && time < GetFinalTimeVolumicSource())
        {
            const auto& volumic_source_operator = GetVolumicSourceOperator();

            GlobalVectorWithCoefficient vector(vector_current_volumic_source, 1.);
            volumic_source_operator.Assemble(std::make_tuple(std::ref(vector)), time);
        }

        GlobalVector& vector_current_time_dependent_source = GetNonCstVectorCurrentTimeDependentSource();
        vector_current_time_dependent_source.ZeroEntries();

        decltype(auto) time_dependent_source_operator = GetTimeDependentSourceOperator();

        {
            GlobalVectorWithCoefficient vector(vector_current_time_dependent_source, 1.);
            time_dependent_source_operator.Assemble(std::make_tuple(std::ref(vector)), time);
        }

        rhs.Copy(vector_current_volumic_source);

        Wrappers::Petsc::AXPY(1., GetVectorCurrentTimeDependentSource(), rhs);

        const GlobalMatrix& stiffness_matrix = GetMatrixStiffness();
        const GlobalMatrix& mass_matrix = GetMatrixMassPerSquareTimeStep();
        const double theta = GetTheta();

        parent::GetNonCstSystemSolution(numbering_subset).UpdateGhosts();
        GetNonCstVectorSystemSolutionTwoIterationsBefore().UpdateGhosts();

        const GlobalVector& system_solution = parent::GetSystemSolution(numbering_subset);

        const GlobalVector& system_solution_two_iterations_before = GetVectorSystemSolutionTwoIterationsBefore();

        Wrappers::Petsc::MatMult(stiffness_matrix, system_solution, vector_current_volumic_source);
        Wrappers::Petsc::AXPY(-(1. - 2. * theta), vector_current_volumic_source, rhs);

        Wrappers::Petsc::MatMult(
            stiffness_matrix, system_solution_two_iterations_before, vector_current_volumic_source);
        Wrappers::Petsc::AXPY(-theta, vector_current_volumic_source, rhs);

        Wrappers::Petsc::MatMult(mass_matrix, system_solution, vector_current_volumic_source);
        Wrappers::Petsc::AXPY(2., vector_current_volumic_source, rhs);

        Wrappers::Petsc::MatMult(mass_matrix, system_solution_two_iterations_before, vector_current_volumic_source);
        Wrappers::Petsc::AXPY(-1., vector_current_volumic_source, rhs);
    }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::UpdateSolutionForNextTimeStep()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();
        const GlobalVector& system_solution = parent::GetSystemSolution(numbering_subset);
        GlobalVector& system_solution_two_iterations_before = GetNonCstVectorSystemSolutionTwoIterationsBefore();
        system_solution_two_iterations_before.Copy(system_solution);
    }


    template<ParameterNS::Type TypeT>
    void VariationalFormulation<TypeT>::ComputeEnergy()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();
        const GlobalVector& system_solution = parent::GetSystemSolution(numbering_subset);
        const GlobalVector& system_solution_two_iterations_before = GetVectorSystemSolutionTwoIterationsBefore();
        const GlobalMatrix& stiffness_matrix = GetMatrixStiffness();
        const GlobalMatrix& mass_matrix = GetMatrixMassPerSquareTimeStep();
        const double theta = GetTheta();
        const double dt = parent::GetTimeManager().GetTimeStep();

        GlobalVector& diff_system_solution = GetNonCstVectorDiffSystemSolution();
        GlobalVector& midpoint_system_solution = GetNonCstVectorMidpointSystemSolution();

        diff_system_solution.Copy(system_solution);
        Wrappers::Petsc::AXPY(-1., system_solution_two_iterations_before, diff_system_solution);
        diff_system_solution.Scale(1. / dt);

        Wrappers::Petsc::MatMult(stiffness_matrix, diff_system_solution, midpoint_system_solution);
        midpoint_system_solution.Scale(theta - 0.25);

        Wrappers::Petsc::MatMultAdd(
            mass_matrix, diff_system_solution, midpoint_system_solution, midpoint_system_solution);

        midpoint_system_solution.Scale(dt * dt);

        double energy = Wrappers::Petsc::DotProduct(diff_system_solution, midpoint_system_solution);

        midpoint_system_solution.Copy(system_solution);
        Wrappers::Petsc::AXPY(1., system_solution_two_iterations_before, midpoint_system_solution);
        midpoint_system_solution.Scale(0.5);

        Wrappers::Petsc::MatMult(stiffness_matrix, midpoint_system_solution, diff_system_solution);

        const double energy_temp = Wrappers::Petsc::DotProduct(midpoint_system_solution, diff_system_solution);

        energy = 0.5 * (energy + energy_temp);

        Wrappers::Petsc::PrintMessageOnFirstProcessor(
            "Energy : %1.16e\n", parent::GetMpi(), std::source_location::current(), energy);

        double diff_energy = energy - midpoint_energy_;

        if (parent::GetTimeManager().GetTime() > dt)
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\nEnergy difference with previous time step : %1.16e\n",
                                                          parent::GetMpi(),
                                                          std::source_location::current(),
                                                          diff_energy);

        midpoint_energy_ = energy;
    }


    template<ParameterNS::Type TypeT>
    inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulation<TypeT>::ImplementSnesFunction() const
    {
        return nullptr;
    }


    template<ParameterNS::Type TypeT>
    inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulation<TypeT>::ImplementSnesJacobian() const
    {
        return nullptr;
    }


    template<ParameterNS::Type TypeT>
    inline Wrappers::Petsc::Snes::SNESViewer VariationalFormulation<TypeT>::ImplementSnesViewer() const
    {
        return nullptr;
    }


    template<ParameterNS::Type TypeT>
    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation<TypeT>::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    template<ParameterNS::Type TypeT>
    inline const NumberingSubset& VariationalFormulation<TypeT>::GetNumberingSubset() const
    {
        return numbering_subset_;
    }


    template<ParameterNS::Type TypeT>
    inline const typename VariationalFormulation<TypeT>::source_operator_type&
    VariationalFormulation<TypeT>::GetVolumicSourceOperator() const noexcept
    {
        assert(!(!volumic_source_operator_));
        return *volumic_source_operator_;
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
    VariationalFormulation<TypeT>::GetStiffnessOperator() const noexcept
    {
        assert(!(!stiffness_operator_));
        return *stiffness_operator_;
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation<TypeT>::GetMassOperator() const noexcept
    {
        assert(!(!mass_operator_) && "Only exists in dynamic");
        return *mass_operator_;
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVector& VariationalFormulation<TypeT>::GetVectorCurrentVolumicSource() const
    {
        assert(!(!vector_current_volumic_source_));
        return *vector_current_volumic_source_;
    }


    template<ParameterNS::Type TypeT>
    inline GlobalVector& VariationalFormulation<TypeT>::GetNonCstVectorCurrentVolumicSource()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentVolumicSource());
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVector& VariationalFormulation<TypeT>::GetVectorCurrentTimeDependentSource() const
    {
        assert(!(!vector_current_time_dependent_source_));
        return *vector_current_time_dependent_source_;
    }


    template<ParameterNS::Type TypeT>
    inline GlobalVector& VariationalFormulation<TypeT>::GetNonCstVectorCurrentTimeDependentSource()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentTimeDependentSource());
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalMatrix& VariationalFormulation<TypeT>::GetMatrixStiffness() const
    {
        assert(!(!matrix_stiffness_));
        return *matrix_stiffness_;
    }


    template<ParameterNS::Type TypeT>
    inline GlobalMatrix& VariationalFormulation<TypeT>::GetNonCstMatrixStiffness()
    {
        return const_cast<GlobalMatrix&>(GetMatrixStiffness());
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalMatrix& VariationalFormulation<TypeT>::GetMatrixMassPerSquareTimeStep() const
    {
        assert(!(!matrix_mass_per_square_time_step_));
        return *matrix_mass_per_square_time_step_;
    }


    template<ParameterNS::Type TypeT>
    inline GlobalMatrix& VariationalFormulation<TypeT>::GetNonCstMatrixMassPerSquareTimeStep()
    {
        return const_cast<GlobalMatrix&>(GetMatrixMassPerSquareTimeStep());
    }


    template<ParameterNS::Type TypeT>
    inline const ScalarParameter<time_manager_type>& VariationalFormulation<TypeT>::GetDensity() const
    {
        assert(!(!density_));
        return *density_;
    }


    template<ParameterNS::Type TypeT>
    inline const ScalarParameter<time_manager_type>& VariationalFormulation<TypeT>::GetDiffusionTensor() const
    {
        assert(!(!diffusion_tensor_));
        return *diffusion_tensor_;
    }


    template<ParameterNS::Type TypeT>
    inline auto VariationalFormulation<TypeT>::GetVolumicSourceParameter() const -> const no_time_dep_param_type&
    {
        assert(!(!volumic_source_parameter_));
        return *volumic_source_parameter_;
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVector& VariationalFormulation<TypeT>::GetVectorSystemSolutionTwoIterationsBefore() const
    {
        assert(!(!system_solution_two_iterations_before_));
        return *system_solution_two_iterations_before_;
    }


    template<ParameterNS::Type TypeT>
    inline GlobalVector& VariationalFormulation<TypeT>::GetNonCstVectorSystemSolutionTwoIterationsBefore()
    {
        return const_cast<GlobalVector&>(GetVectorSystemSolutionTwoIterationsBefore());
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVector& VariationalFormulation<TypeT>::GetVectorDiffSystemSolution() const
    {
        assert(!(!diff_system_solution_));
        return *diff_system_solution_;
    }


    template<ParameterNS::Type TypeT>
    inline GlobalVector& VariationalFormulation<TypeT>::GetNonCstVectorDiffSystemSolution()
    {
        return const_cast<GlobalVector&>(GetVectorDiffSystemSolution());
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVector& VariationalFormulation<TypeT>::GetVectorMidpointSystemSolution() const
    {
        assert(!(!midpoint_system_solution_));
        return *midpoint_system_solution_;
    }


    template<ParameterNS::Type TypeT>
    inline GlobalVector& VariationalFormulation<TypeT>::GetNonCstVectorMidpointSystemSolution()
    {
        return const_cast<GlobalVector&>(GetVectorMidpointSystemSolution());
    }


    template<ParameterNS::Type TypeT>
    inline double VariationalFormulation<TypeT>::GetTheta() const noexcept
    {
        return theta_;
    }


    template<ParameterNS::Type TypeT>
    inline double VariationalFormulation<TypeT>::GetInitialTimeVolumicSource() const noexcept
    {
        return initial_time_volumic_source_;
    }


    template<ParameterNS::Type TypeT>
    inline double VariationalFormulation<TypeT>::GetFinalTimeVolumicSource() const noexcept
    {
        return final_time_volumic_source_;
    }


    template<ParameterNS::Type TypeT>
    inline auto VariationalFormulation<TypeT>::GetTimeDependentSourceOperator() const noexcept -> const source_op_type&
    {
        assert(!(!time_dependent_source_operator_));
        return *time_dependent_source_operator_;
    }


    template<ParameterNS::Type TypeT>
    inline auto VariationalFormulation<TypeT>::GetNonCstTimeDependentSourceOperator() noexcept -> source_op_type&
    {
        return const_cast<source_op_type&>(GetTimeDependentSourceOperator());
    }


    template<ParameterNS::Type TypeT>
    inline auto VariationalFormulation<TypeT>::GetTimeDependentSourceParameter() const noexcept
        -> const time_dependent_parameter_type&
    {
        assert(!(!time_dependent_source_parameter_));
        return *time_dependent_source_parameter_;
    }


    template<ParameterNS::Type TypeT>
    inline auto VariationalFormulation<TypeT>::GetNonCstTimeDependentSourceParameter() noexcept
        -> time_dependent_parameter_type&
    {
        return const_cast<time_dependent_parameter_type&>(GetTimeDependentSourceParameter());
    }


} // namespace MoReFEM::AcousticWaveNS


// > *** MoReFEM4AcousticWave end header guards *** //
#endif // MOREFEM4ACOUSTICWAVE_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM4AcousticWave end header guards *** < //
