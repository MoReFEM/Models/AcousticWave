// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //

// > *** MoReFEM4AcousticWave header guards *** //
#ifndef MOREFEM4ACOUSTICWAVE_MODEL_DOT_HXX_
#define MOREFEM4ACOUSTICWAVE_MODEL_DOT_HXX_
// IWYU pragma: private, include "Model.hpp"
// *** MoReFEM4AcousticWave header guards *** < //

namespace MoReFEM::AcousticWaveNS
{


    template<ParameterNS::Type TypeT>
    Model<TypeT>::Model(morefem_data_type<TypeT>& morefem_data) : parent(morefem_data)
    { }


    template<ParameterNS::Type TypeT>
    void Model<TypeT>::SupplInitialize()
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        const auto& numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        decltype(auto) time_manager = parent::GetNonCstTimeManager();

        const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance();

        auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
            AsBoundaryConditionId(BoundaryConditionIndex::first)

                ) };

        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();

        variational_formulation_ = std::make_unique<VariationalFormulation<TypeT>>(
            numbering_subset, god_of_dof, std::move(bc_list), morefem_data);


        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);
        variational_formulation.WriteSolution(time_manager, numbering_subset);
    }


    template<ParameterNS::Type TypeT>
    void Model<TypeT>::Forward()
    {
        auto& variational_formulation = this->GetNonCstVariationalFormulation();

        // Only Rhs is modified at each time iteration; compute it and solve the system.
        variational_formulation.ComputeDynamicSystemRhs();

        variational_formulation.UpdateSolutionForNextTimeStep();

        const NumberingSubset& numbering_subset = variational_formulation.GetNumberingSubset();

        if (parent::GetTimeManager().NtimeModified() == 1)
        {
            variational_formulation
                .template ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
                    numbering_subset, numbering_subset);
            variational_formulation.template SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset);
        } else
        {
            variational_formulation.template ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(
                numbering_subset, numbering_subset);
            variational_formulation.template SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset);
        }
    }


    template<ParameterNS::Type TypeT>
    void Model<TypeT>::SupplFinalizeStep()
    {
        // Update quantities for next iteration.
        auto& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& numbering_subset = variational_formulation.GetNumberingSubset();

        variational_formulation.WriteSolution(parent::GetTimeManager(), numbering_subset);

        variational_formulation.ComputeEnergy();
    }


    template<ParameterNS::Type TypeT>
    void Model<TypeT>::SupplFinalize()
    { }


    template<ParameterNS::Type TypeT>
    inline const std::string& Model<TypeT>::ClassName()
    {
        static std::string name("AcousticWave");
        return name;
    }


    template<ParameterNS::Type TypeT>
    inline bool Model<TypeT>::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    template<ParameterNS::Type TypeT>
    inline void Model<TypeT>::SupplInitializeStep()
    {
        GetNonCstVariationalFormulation().GetNonCstTimeDependentSourceParameter().TimeUpdate();
    }


    template<ParameterNS::Type TypeT>
    inline const VariationalFormulation<TypeT>& Model<TypeT>::GetVariationalFormulation() const noexcept
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    template<ParameterNS::Type TypeT>
    inline VariationalFormulation<TypeT>& Model<TypeT>::GetNonCstVariationalFormulation() noexcept
    {
        return const_cast<VariationalFormulation<TypeT>&>(GetVariationalFormulation());
    }


} // namespace MoReFEM::AcousticWaveNS

// > *** MoReFEM4AcousticWave end header guards *** //
#endif // MOREFEM4ACOUSTICWAVE_MODEL_DOT_HXX_
// *** MoReFEM4AcousticWave end header guards *** < //
