// > *** MoReFEM4AcousticWave copyright notice *** //
/*!
 * This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
 * to keep it working with latest MoReFEM version.
 *
 * \file
 *
 */
// *** MoReFEM4AcousticWave copyright notice *** < //

// > *** MoReFEM4AcousticWave header guards *** //
#ifndef MOREFEM4ACOUSTICWAVE_MODEL_DOT_HPP_
#define MOREFEM4ACOUSTICWAVE_MODEL_DOT_HPP_
// *** MoReFEM4AcousticWave header guards *** < //

#include <memory>
#include <vector>

#include "Model/Model.hpp"

#include "InputData.hpp"
#include "VariationalFormulation.hpp"


namespace MoReFEM::AcousticWaveNS
{


    /*!
     * \brief Model that implements the acoustic problem in static and dynamic.
     *
     * \tparam TypeT Whether the unknown is scalar or vectorial.
     */
    template<ParameterNS::Type TypeT>
    class Model
    : public MoReFEM::Model<Model<TypeT>, morefem_data_type<TypeT>, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model<TypeT>;

        //! Convenient alias.
        using parent = MoReFEM::Model<self, morefem_data_type<TypeT>, DoConsiderProcessorWiseLocal2Global::yes>;

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        Model(morefem_data_type<TypeT>& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! Copy constructor.
        Model(const Model&) = delete;

        //! Move constructor.
        Model(Model&&) = delete;

        //! Copy affectation.
        Model& operator=(const Model&) = delete;

        //! Move affectation.
        Model& operator=(Model&&) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        //! Manage time iteration.
        void Forward();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();

      private:
        //! Non constant access to the underlying VariationalFormulation object.
        VariationalFormulation<TypeT>& GetNonCstVariationalFormulation() noexcept;

        //! Access to the underlying VariationalFormulation object.
        const VariationalFormulation<TypeT>& GetVariationalFormulation() const noexcept;


      private:
        /*!
         * \brief Whether the model wants to add additional cases in which the Model stops (besides the reach of
         * maximum time).
         *
         * Returns always true (no such additional condition in this Model).
         */
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();


        ///@}

      private:
        //! Underlying variational formulation.
        typename VariationalFormulation<TypeT>::unique_ptr variational_formulation_ = nullptr;
    };


} // namespace MoReFEM::AcousticWaveNS


#include "Model.hxx"


// > *** MoReFEM4AcousticWave end header guards *** //
#endif // MOREFEM4ACOUSTICWAVE_MODEL_DOT_HPP_
// *** MoReFEM4AcousticWave end header guards *** < //
