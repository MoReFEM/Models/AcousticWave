# > *** MoReFEM4AcousticWave copyright notice *** #
#######################################################################################################################
# This file is an acoustic wave model using [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#
# MoReFEM4RAcousticWave is loosely maintained by Sébastien Gilles (sebastien.gilles@inria.fr), who does minimal updates
# to keep it working with latest MoReFEM version.
#######################################################################################################################
# *** MoReFEM4AcousticWave copyright notice *** < #

import importlib.util
import os
import sys

if "MOREFEM_INSTALL_PREFIX" not in os.environ:
    raise Exception("Environment variable MOREFEM_INSTALL_PREFIX should be defined! (and point to the location where MoReREM CoreLibrary was installed - through the `ninja install` command!)")


module_name = "run_clang_format"
module_path = os.path.join(os.environ["MOREFEM_INSTALL_PREFIX"], "share", "Scripts", "Tools", "run_clang_format.py")
spec = importlib.util.spec_from_file_location(module_name, module_path)
module = importlib.util.module_from_spec(spec)
sys.modules[module_name] = module
spec.loader.exec_module(module)

from run_clang_format import ApplyClangFormat

if "EXTERNAL_MODEL_ROOT" not in os.environ:
    raise Exception("Environment variable EXTERNAL_MODEL_ROOT is not defined; please do so! (should point to the location where AcousticWave model was cloned.)")

modified_files_list = ApplyClangFormat(os.path.join(os.environ["EXTERNAL_MODEL_ROOT"]), pause_after_each_directory = False)

if len(modified_files_list) == 0:
    print("No files modified!")
    sys.exit(0)
else:
    print(f"The {len(modified_files_list)} following files were modified:")
    print("\n\t- {}".format("\n\t- ".join(modified_files_list)))
    sys.exit(1)