if [[ -z "${MOREFEM_INSTALL_PREFIX}" ]]; then
    echo "Environment variable MOREFEM_INSTALL_PREFIX is not defined; please do so! (should point to the destination in which MoREFEM library was installed.)"
    exit 1
fi

if [[ -z "${EXTERNAL_MODEL_ROOT}" ]]; then
    echo "Environment variable EXTERNAL_MODEL_ROOT is not defined; please do so! (should point to the location where AcousticWave model was cloned.)"
    exit 1
fi


python ${MOREFEM_INSTALL_PREFIX}/share/Scripts/Tools/generate_automated_blocks_content_in_source_files.py --project=MoReFEM4AcousticWave --directory=${EXTERNAL_MODEL_ROOT}/Sources --copyright_notice=${EXTERNAL_MODEL_ROOT}/share/CopyrightNotice.txt 

python ${MOREFEM_INSTALL_PREFIX}/share/Scripts/Tools/generate_automated_blocks_content_in_source_files.py --project=MoReFEM4AcousticWave --directory=${EXTERNAL_MODEL_ROOT}/Scripts --copyright_notice=${EXTERNAL_MODEL_ROOT}/share/CopyrightNotice.txt 

