if [[ -z "${MOREFEM_INSTALL_PREFIX}" ]]; then
    echo "Environment variable MOREFEM_INSTALL_PREFIX is not defined; please do so! (should point to the destination in which MoREFEM library was installed.)"
    exit 1
fi

if [[ -z "${ACOUSTIC_WAVE_ROOT}" ]]; then
    echo "Environment variable ACOUSTIC_WAVE_ROOT is not defined; please do so! (should point to the location where AcousticWave model was cloned.)"
    exit 1
fi

python ${MOREFEM_INSTALL_PREFIX}/share/Scripts/Tools/new_file.py --project=MoReFEM4AcousticWave --sources_dir=${ACOUSTIC_WAVE_ROOT}/Sources --copyright_notice=${MOREFEM_INSTALL_PREFIX}/share/CopyrightNotice.txt --project_namespace=MoReFEM::AcousticWave
