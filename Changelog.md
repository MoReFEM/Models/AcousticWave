# v24.42

- Update to work with MoReFEM v24.42.

- Set the project so that it might use some of the CoreLibrary scripts (to create a new file, update the specific parts such as header and banner at the top of the file, run clang-format, etc...)
- Refactor CMake so that it uses up the custom macro defined in CoreLibrary.
- Remove XCode projects, and ensure generation of an XCode project with CMake works.
- Refresh CI.

# v24.04

- Update to work with MoReFEM v24.04.

# v24.03

- Update to work with MoReFEM v24.03.

# v23.45

- Update to work with MoReFEM v23.45.
- Deactivate macOS for the time being (no runner available).

# v23.37

- Update to work with MoReFEM v23.37. This entails using the brand new `ModelSettings` facility for input data.

# v23.12

- Update to work with MoReFEM v23.11.2.

# v22.37

- Update to MoReFEM v22.37.
- Use SuperLU_dist rather than Mumps due to issue with the latter in parallel (see [this MoReFEM ticket](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1705) for more details.)
- Add tags for Gitlab Inria shared runners (see [this MoReFEM ticket](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1710) for more details.)


# v21.28

- Update to MoReFEM v21.27.
- Fix XCode build (requires this [MoReFEM ticket](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1663) not yet in MoReFEM v21.27).

# v21.23

- Update to MoReFEM v21.22.
- CI: remove tags for Docker builds (as shared runners are now used).

# v21.07

- Update to MoReFEM v21.06: two includes have been modified.

# v21.01.2

- Fix a warning that appears with most recent versions of clang (on Fedora)

# v21.01

- Update to MoReFEM v20.52:
  . Field 'may_overlap' in lua file for boundary condition has been removed.
  . Reference Ensight mesh file have been modified to reflect the change of convention used (node id given rather than assign).

# v20.18

- Update to new MoReFEM API: modify the expected "interfaces.hhdata" files so that they follow new convention, and check them only in sequential mode (as parallel one is now only a subset of this file for each rank).

# v20.12:

- Remove DebugPrintNorm() call, which was removed in MoReFEM core library.
- Modify the registry from which the MoReFEM base images should be fetched (until MoReFEM #1509 is resolved)

# v19.52:

- Feature #1: introduce CI.


# v19.45.1:

- Update to the new MoReFEM API.

# v19.27:

- Update to the new MoReFEM API and adapt some expected results files accordingly.
- Also adapt Lua files.


# v19.16:

- Update to the new MoReFEM API (especially tests now handled with Boost.Test).
- Fix the README.

# v18.47:


- MoReFEM Feature #1375 Remove legacy models.
- MoReFEM Feature #1343 Add integration tests. Currently only for not to time consuming models...
- MoReFEM Support #1350 Update the models to match the changes introduced concerning TimeManager management within a Model.
- MoReFEM Support #1374: Replace MOREFEM_ROOT by more accurate MODEL_ROOT_DIRECTORY.
- MoReFEM Support #1373: Update the model to the v18.47 MoReFEM API.
- **MoReFEM Support #1371**: Propagate this ticket from MoReFEM library that rename the InputParameterList InputData.

# v18.32:

- MoReFEM Feature #1318: Add integration test.


# v18.30:

- MoReFEM Support #1309: Update the Lua files to the simplified new interface for parameters.

# v18.27

- Change paths and settings to make it compliant with MoReFEM v18.27.

# v18.12.2

- MoReFEM Support #1250: Update AcousticWave model to work with MoReFEM v18.12.2. CMake scripts have been polished slightly, and SCons build entirely removed.
